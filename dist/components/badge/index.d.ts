import { PropsWithChildren, HTMLAttributes } from 'react';
export interface BadgeProps extends HTMLAttributes<HTMLDivElement> {
    /** 状态色*/
    status?: 'positive' | 'negative' | 'neutral' | 'warning' | 'error';
}
export declare function Badge(props: PropsWithChildren<BadgeProps>): JSX.Element;
export declare namespace Badge {
    var defaultProps: {
        status: string;
    };
}
export default Badge;
