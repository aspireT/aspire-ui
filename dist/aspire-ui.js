function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var React = require('react');
var React__default = _interopDefault(React);
var styled = require('styled-components');
var styled__default = _interopDefault(styled);
var polished = require('polished');
var Prism = _interopDefault(require('prismjs'));

function _taggedTemplateLiteralLoose(strings, raw) {
  if (!raw) {
    raw = strings.slice(0);
  }

  strings.raw = raw;
  return strings;
}

// import { css } from "styled-components";
var color = {
  // Palette
  primary: '#FF4785',
  secondary: '#1EA7FD',
  tertiary: '#DDDDDD',
  orange: '#FC521F',
  gold: '#FFAE00',
  green: '#66BF3C',
  seafoam: '#37D5D3',
  purple: '#6F2CAC',
  ultraviolet: '#2A0481',
  // Monochrome
  lightest: '#FFFFFF',
  lighter: '#F8F8F8',
  light: '#F3F3F3',
  mediumlight: '#EEEEEE',
  medium: '#DDDDDD',
  mediumdark: '#999999',
  dark: '#666666',
  darker: '#444444',
  darkest: '#333333',
  border: 'rgba(0,0,0,.1)',
  // Status
  positive: '#66BF3C',
  negative: '#FF4400',
  warning: '#E69D00'
};
var background = {
  app: '#F6F9FC',
  appInverse: '#7A8997',
  positive: '#E1FFD4',
  negative: '#FEDED2',
  warning: '#FFF5CF'
};
var typography = {
  type: {
    primary: '"Nunito Sans", "Helvetica Neue", Helvetica, Arial, sans-serif',
    //书写字体
    code: '"SFMono-Regular", Consolas, "Liberation Mono", Menlo, Courier, monospace'
  },
  weight: {
    regular: '400',
    bold: '700',
    extrabold: '800',
    black: '900'
  },
  size: {
    s1: '12',
    s2: '14',
    s3: '16',
    m1: '20',
    m2: '24',
    m3: '28',
    l1: '32',
    l2: '40',
    l3: '48'
  }
};
var spacing = {
  padding: {
    small: 10,
    medium: 20,
    large: 30
  },
  borderRadius: {
    small: 5,
    default: 10
  }
};
var breakpoint = 600;
var pageMargin = 5;
var badgeColor = {
  positive: color.positive,
  negative: color.negative,
  neutral: color.dark,
  warning: color.warning,
  error: color.lightest
};
var badgeBackground = {
  positive: background.positive,
  negative: background.negative,
  neutral: color.mediumlight,
  warning: background.warning,
  error: color.negative
};

function _templateObject() {
  var data = _taggedTemplateLiteralLoose(["\n  0%, 100% { opacity: 1; }\n  50% { opacity: .4; }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}
var easing = {
  rubber: 'cubic-bezier(0.175, 0.885, 0.335, 1.05)'
};
var glow = styled.keyframes(_templateObject());

function _templateObject3() {
  var data = _taggedTemplateLiteralLoose(["\n  border: 0;\n  border-radius: 3em;\n  cursor: pointer;\n  display: inline-block;\n  overflow: hidden;\n  padding: ", ";\n  position: relative;\n  text-align: center;\n  text-decoration: none;\n  transition: all 150ms ease-out;\n  transform: translate3d(0,0,0);\n  vertical-align: top;\n  white-space: nowrap;\n  user-select: none;\n  opacity: 1;\n  margin: 0;\n  background: transparent;\n\n\n  font-size: ", "px;\n  font-weight: ", ";\n  line-height: 1;\n\n  ", "\n\n  ", " {\n    transform: scale3d(1,1,1) translate3d(0,0,0);\n    transition: transform 700ms ", ";\n    opacity: 1;\n  }\n\n  ", " {\n    transform: translate3d(0, 100%, 0);\n  }\n\n  ", "\n\n  ", "\n\n  ", "\n\n  ", "\n\n  ", "\n\n  ", "\n\n  ", ";\n\n    ", ";\n\n    ", ";\n\n      ", "\n\n      ", "\n\n      ", ";\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteralLoose(["\n  position: absolute;\n  top: 50%;\n  left: 0;\n  right: 0;\n  opacity: 0;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject$1() {
  var data = _taggedTemplateLiteralLoose(["\n  display: inline-block;\n  vertical-align: top;\n"]);

  _templateObject$1 = function _templateObject() {
    return data;
  };

  return data;
}
var APPEARANCES = {
  primary: 'primary',
  primaryOutline: 'primaryOutline',
  secondary: 'secondary',
  secondaryOutline: 'secondaryOutline',
  tertiary: 'tertiary',
  outline: 'outline',
  inversePrimary: 'inversePrimary',
  inverseSecondary: 'inverseSecondary',
  inverseOutline: 'inverseOutline'
};
var SIZES = {
  small: 'small',
  medium: 'medium'
};
var btnpadding = {
  small: '8px 16px',
  medium: '13px 20px'
};
var Text = styled__default.span(_templateObject$1());
var Loading = styled__default.span(_templateObject2());
var StyledButton = styled__default.button(_templateObject3(), function (props) {
  return props.size === SIZES.small ? '8px 16px' : '13px 20px';
}, function (props) {
  return props.size === SIZES.small ? typography.size.s1 : typography.size.s2;
}, typography.weight.extrabold, function (props) {
  return !props.isLoading && "\n      &:hover {\n        transform: translate3d(0, -2px, 0);\n        box-shadow: rgba(0, 0, 0, 0.2) 0 2px 6px 0;\n      }\n\n      &:active {\n        transform: translate3d(0, 0, 0);\n      }\n\n      &:focus {\n        box-shadow: " + polished.rgba(color.primary, 0.4) + " 0 1px 9px 2px;\n      }\n\n      &:focus:hover {\n        box-shadow: " + polished.rgba(color.primary, 0.2) + " 0 8px 18px 0px;\n      }\n    ";
}, Text, easing.rubber, Loading, function (props) {
  return props.disabled && "\n      cursor: not-allowed !important;\n      opacity: 0.5;\n      &:hover {\n        transform: none;\n      }\n    ";
}, function (props) {
  return props.isUnclickable && "\n      cursor: default !important;\n      pointer-events: none;\n      &:hover {\n        transform: none;\n      }\n    ";
}, function (props) {
  return props.isLoading && "\n      cursor: progress !important;\n      opacity: 0.7;\n\n      " + Loading + " {\n        transition: transform 700ms " + easing.rubber + ";\n        transform: translate3d(0, -50%, 0);\n        opacity: 1;\n      }\n\n      " + Text + " {\n        transform: scale3d(0, 0, 1) translate3d(0, -100%, 0);\n        opacity: 0;\n      }\n\n      &:hover {\n        transform: none;\n      }\n    ";
}, function (props) {
  return props.appearance === APPEARANCES.primary && "\n      background: " + color.primary + ";\n      color: " + color.lightest + ";\n\n      " + (!props.isLoading && "\n          &:hover {\n            background: " + polished.darken(0.05, color.primary) + ";\n          }\n          &:active {\n            box-shadow: rgba(0, 0, 0, 0.1) 0 0 0 3em inset;\n          }\n          &:focus {\n            box-shadow: " + polished.rgba(color.primary, 0.4) + " 0 1px 9px 2px;\n          }\n          &:focus:hover {\n            box-shadow: " + polished.rgba(color.primary, 0.2) + " 0 8px 18px 0px;\n          }\n        ") + "\n    ";
}, function (props) {
  return props.appearance === APPEARANCES.secondary && "\n      background: " + color.secondary + ";\n      color: " + color.lightest + ";\n\n      " + (!props.isLoading && "\n          &:hover {\n            background: " + polished.darken(0.05, color.secondary) + ";\n          }\n          &:active {\n            box-shadow: rgba(0, 0, 0, 0.1) 0 0 0 3em inset;\n          }\n          &:focus {\n            box-shadow: " + polished.rgba(color.secondary, 0.4) + " 0 1px 9px 2px;\n          }\n          &:focus:hover {\n            box-shadow: " + polished.rgba(color.secondary, 0.2) + " 0 8px 18px 0px;\n          }\n        ") + "\n    ";
}, function (props) {
  return props.appearance === APPEARANCES.tertiary && "\n      background: " + color.tertiary + ";\n      color: " + color.darkest + ";\n\n      " + (!props.isLoading && "\n          &:hover {\n            background: " + polished.darken(0.05, color.tertiary) + ";\n          }\n          &:active {\n            box-shadow: rgba(0, 0, 0, 0.1) 0 0 0 3em inset;\n          }\n          &:focus {\n            box-shadow: " + polished.rgba(color.darkest, 0.15) + " 0 1px 9px 2px;\n          }\n          &:focus:hover {\n            box-shadow: " + polished.rgba(color.darkest, 0.05) + " 0 8px 18px 0px;\n          }\n        ") + "\n    ";
}, function (props) {
  return props.appearance === APPEARANCES.outline && "\n      box-shadow: " + polished.opacify(0.05, color.border) + " 0 0 0 1px inset;\n      color: " + color.dark + ";\n      background: transparent;\n\n      " + (!props.isLoading && "\n          &:hover {\n            box-shadow: " + polished.opacify(0.3, color.border) + " 0 0 0 1px inset;\n          }\n\n          &:active {\n            background: " + polished.opacify(0.05, color.border) + ";\n            box-shadow: transparent 0 0 0 1px inset;\n            color: " + color.darkest + ";\n          }\n\n          &:active:focus:hover {\n            " + "\n            background: " + polished.opacify(0.05, color.border) + ";\n            box-shadow:  " + polished.rgba(color.darkest, 0.15) + " 0 1px 9px 2px;\n          }\n\n          &:focus {\n            box-shadow: " + polished.opacify(0.05, color.border) + " 0 0 0 1px inset, \n            " + polished.rgba(color.darkest, 0.15) + " 0 1px 9px 2px;\n          }\n          &:focus:hover {\n            box-shadow: " + polished.opacify(0.05, color.border) + " 0 0 0 1px inset, \n            " + polished.rgba(color.darkest, 0.05) + " 0 8px 18px 0px;\n          }\n        ") + ";\n    ";
}, function (props) {
  return props.appearance === APPEARANCES.primaryOutline && "\n        box-shadow: " + color.primary + " 0 0 0 1px inset;\n        color: " + color.primary + ";\n\n        &:hover {\n          box-shadow: " + color.primary + " 0 0 0 1px inset;\n          background: transparent;\n        }\n\n        &:active {\n          background: " + color.primary + ";\n          box-shadow: " + color.primary + " 0 0 0 1px inset;\n          color: " + color.lightest + ";\n        }\n        &:focus {\n          box-shadow: " + color.primary + " 0 0 0 1px inset, " + polished.rgba(color.primary, 0.4) + " 0 1px 9px 2px;\n        }\n        &:focus:hover {\n          box-shadow: " + color.primary + " 0 0 0 1px inset, " + polished.rgba(color.primary, 0.2) + " 0 8px 18px 0px;\n        }\n      ";
}, function (props) {
  return props.appearance === APPEARANCES.secondaryOutline && "\n        box-shadow: " + color.secondary + " 0 0 0 1px inset;\n        color: " + color.secondary + ";\n\n        &:hover {\n          box-shadow: " + color.secondary + " 0 0 0 1px inset;\n          background: transparent;\n        }\n\n        &:active {\n          background: " + color.secondary + ";\n          box-shadow: " + color.secondary + " 0 0 0 1px inset;\n          color: " + color.lightest + ";\n        }\n        &:focus {\n          box-shadow: " + color.secondary + " 0 0 0 1px inset,\n            " + polished.rgba(color.secondary, 0.4) + " 0 1px 9px 2px;\n        }\n        &:focus:hover {\n          box-shadow: " + color.secondary + " 0 0 0 1px inset,\n            " + polished.rgba(color.secondary, 0.2) + " 0 8px 18px 0px;\n        }\n      ";
}, function (props) {
  return props.appearance === APPEARANCES.inversePrimary && "\n          background: " + color.lightest + ";\n          color: " + color.primary + ";\n\n          " + (!props.isLoading && "\n              &:hover {\n                background: " + color.lightest + ";\n              }\n              &:active {\n                box-shadow: rgba(0, 0, 0, 0.1) 0 0 0 3em inset;\n              }\n              &:focus {\n                box-shadow: " + polished.rgba(color.primary, 0.4) + " 0 1px 9px 2px;\n              }\n              &:focus:hover {\n                box-shadow: " + polished.rgba(color.primary, 0.2) + " 0 8px 18px 0px;\n              }\n          ") + "\n      ";
}, function (props) {
  return props.appearance === APPEARANCES.inverseSecondary && "\n          background: " + color.lightest + ";\n          color: " + color.secondary + ";\n\n          " + (!props.isLoading && "\n              &:hover {\n                background: " + color.lightest + ";\n              }\n              &:active {\n                box-shadow: rgba(0, 0, 0, 0.1) 0 0 0 3em inset;\n              }\n              &:focus {\n                box-shadow: " + polished.rgba(color.secondary, 0.4) + " 0 1px 9px 2px;\n              }\n              &:focus:hover {\n                box-shadow: " + polished.rgba(color.secondary, 0.2) + " 0 8px 18px 0px;\n              }\n          ") + "\n      ";
}, function (props) {
  return props.appearance === APPEARANCES.inverseOutline && "\n          box-shadow: " + color.lightest + " 0 0 0 1px inset;\n          color: " + color.lightest + ";\n\n          &:hover {\n            box-shadow: " + color.lightest + " 0 0 0 1px inset;\n            background: transparent;\n          }\n\n          &:active {\n            background: " + color.lightest + ";\n            box-shadow: " + color.lightest + " 0 0 0 1px inset;\n            color: " + color.darkest + ";\n          }\n          &:focus {\n            box-shadow: " + color.lightest + " 0 0 0 1px inset,\n              " + polished.rgba(color.darkest, 0.4) + " 0 1px 9px 2px;\n          }\n          &:focus:hover {\n            box-shadow: " + color.lightest + " 0 0 0 1px inset,\n              " + polished.rgba(color.darkest, 0.2) + " 0 8px 18px 0px;\n          }\n      ";
}); // interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {}

function _templateObject$2() {
  var data = _taggedTemplateLiteralLoose(["\n  .token.cdata,\n  .token.comment,\n  .token.doctype,\n  .token.prolog {\n    color: #708090;\n  }\n  .token.punctuation {\n    color: #999;\n  }\n  .namespace {\n    opacity: 0.7;\n  }\n  .token.boolean,\n  .token.constant,\n  .token.deleted,\n  .token.number,\n  .token.property,\n  .token.symbol,\n  .token.tag {\n    color: #905;\n  }\n  .token.attr-name,\n  .token.builtin,\n  .token.char,\n  .token.inserted,\n  .token.selector,\n  .token.string {\n    color: #690;\n  }\n  .language-css .token.string,\n  .style .token.string,\n  .token.entity,\n  .token.operator,\n  .token.url {\n    color: #a67f59;\n    background: hsla(0, 0%, 100%, 0.5);\n  }\n  .token.atrule,\n  .token.attr-value,\n  .token.keyword {\n    color: #07a;\n  }\n  .token.class-name,\n  .token.function {\n    color: #dd4a68;\n  }\n  .token.important,\n  .token.regex,\n  .token.variable {\n    color: #e90;\n  }\n  .token.bold,\n  .token.important {\n    font-weight: 700;\n  }\n  .token.italic {\n    font-style: italic;\n  }\n  .token.entity {\n    cursor: help;\n  }\n\n  code,\n  pre {\n    color: ", ";\n  }\n\n  code {\n    white-space: pre;\n  }\n\n  pre {\n    padding: 11px 1rem;\n    margin: 1rem 0;\n    background: ", ";\n    overflow: auto;\n  }\n\n  .language-bash .token.operator,\n  .language-bash .token.function,\n  .language-bash .token.builtin {\n    color: ", ";\n    background: none;\n  }\n"]);

  _templateObject$2 = function _templateObject() {
    return data;
  };

  return data;
}

require("prismjs/components/prism-typescript");

require("prismjs/components/prism-javascript");

require("prismjs/components/prism-json");

require("prismjs/components/prism-css");

var color$1 = {
  darkest: "#666",
  lighter: "#ffff"
};
var HighlightBlock = styled__default.div(_templateObject$2(), color$1.darkest, color$1.lighter, color$1.darkest);
var htmlEscapes = {
  "&": "&amp;",
  "<": "&lt;",
  ">": "&gt;",
  '"': "&quot;",
  "'": "&#39;"
};
var reUnescapedHtml = /[&<>"']/g;
var reHasUnescapedHtml = RegExp(reUnescapedHtml.source);

function escape(string) {
  return string && reHasUnescapedHtml.test(string) ? string.replace(reUnescapedHtml, function (chr) {
    return htmlEscapes[chr];
  }) : string || "";
}

function Highlight(props) {
  var nodeRef = React.useRef(null);
  var children = props.children,
      language = props.language;
  var codeBlock = React__default.createElement("div", {
    dangerouslySetInnerHTML: {
      __html: escape(children + "")
    }
  });
  React.useEffect(function () {
    if (nodeRef.current) {
      Prism.highlightAllUnder(nodeRef.current);
    }
  }, [nodeRef]);
  return React__default.createElement(HighlightBlock, {
    ref: nodeRef
  }, React__default.createElement("pre", {
    className: "language-" + language
  }, React__default.createElement("code", {
    className: "language-" + language
  }, codeBlock)));
}

function _templateObject$3() {
  var data = _taggedTemplateLiteralLoose(["\nhtml {\n  line-height: 1.15; /* 1 */\n  -webkit-text-size-adjust: 100%; /* 2 */\n}\n\nbody {\n  margin: 0;\n}\n\nmain {\n  display: block;\n}\n\nh1 {\n  font-size: 2em;\n  margin: 0.67em 0;\n}\n\nhr {\n  box-sizing: content-box;\n  height: 0;\n  overflow: visible;\n}\n\npre {\n  font-family: monospace, monospace;\n  font-size: 1em;\n}\n\na {\n  background-color: transparent;\n}\n\nabbr[title] {\n  border-bottom: none;\n  text-decoration: underline;\n  text-decoration: underline dotted;\n}\n\nb,\nstrong {\n  font-weight: bolder;\n}\n\ncode,\nkbd,\nsamp {\n  font-family: monospace, monospace;\n  font-size: 1em;\n}\n\nsmall {\n  font-size: 80%;\n}\n\nsub,\nsup {\n  font-size: 75%;\n  line-height: 0;\n  position: relative;\n  vertical-align: baseline;\n}\n\nsub {\n  bottom: -0.25em;\n}\n\nsup {\n  top: -0.5em;\n}\n\nimg {\n  border-style: none;\n}\n\nbutton,\ninput,\noptgroup,\nselect,\ntextarea {\n  font-family: inherit;\n  font-size: 100%;\n  line-height: 1.15;\n  margin: 0;\n}\n\nbutton,\ninput {\n  overflow: visible;\n}\n\nbutton,\nselect {\n  text-transform: none;\n}\n\nbutton,\n[type=\"button\"],\n[type=\"reset\"],\n[type=\"submit\"] {\n  -webkit-appearance: button;\n}\n\nbutton::-moz-focus-inner,\n[type=\"button\"]::-moz-focus-inner,\n[type=\"reset\"]::-moz-focus-inner,\n[type=\"submit\"]::-moz-focus-inner {\n  border-style: none;\n  padding: 0;\n}\n\nbutton:-moz-focusring,\n[type=\"button\"]:-moz-focusring,\n[type=\"reset\"]:-moz-focusring,\n[type=\"submit\"]:-moz-focusring {\n  outline: 1px dotted ButtonText;\n}\n\nfieldset {\n  padding: 0.35em 0.75em 0.625em;\n}\n\nlegend {\n  box-sizing: border-box; /* 1 */\n  color: inherit; /* 2 */\n  display: table; /* 1 */\n  max-width: 100%; /* 1 */\n  padding: 0; /* 3 */\n  white-space: normal; /* 1 */\n}\n\nprogress {\n  vertical-align: baseline;\n}\n\ntextarea {\n  overflow: auto;\n}\n\n[type=\"checkbox\"],\n[type=\"radio\"] {\n  box-sizing: border-box; /* 1 */\n  padding: 0; /* 2 */\n}\n\n[type=\"number\"]::-webkit-inner-spin-button,\n[type=\"number\"]::-webkit-outer-spin-button {\n  height: auto;\n}\n\n[type=\"search\"] {\n  -webkit-appearance: textfield; /* 1 */\n  outline-offset: -2px; /* 2 */\n}\n\n[type=\"search\"]::-webkit-search-decoration {\n  -webkit-appearance: none;\n}\n\n::-webkit-file-upload-button {\n  -webkit-appearance: button; /* 1 */\n  font: inherit; /* 2 */\n}\n\ndetails {\n  display: block;\n}\n\nsummary {\n  display: list-item;\n}\n\ntemplate {\n  display: none;\n}\n\n[hidden] {\n  display: none;\n}\n"]);

  _templateObject$3 = function _templateObject() {
    return data;
  };

  return data;
}
var GlobalStyle = styled.createGlobalStyle(_templateObject$3());

exports.APPEARANCES = APPEARANCES;
exports.GlobalStyle = GlobalStyle;
exports.Highlight = Highlight;
exports.SIZES = SIZES;
exports.background = background;
exports.badgeBackground = badgeBackground;
exports.badgeColor = badgeColor;
exports.breakpoint = breakpoint;
exports.btnpadding = btnpadding;
exports.color = color;
exports.easing = easing;
exports.glow = glow;
exports.pageMargin = pageMargin;
exports.spacing = spacing;
exports.typography = typography;
//# sourceMappingURL=aspire-ui.js.map
